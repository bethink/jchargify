package com.jon.chargify.customer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

import com.google.gson.Gson;
import com.jon.chargify.Chargify;
import com.jon.chargify.payment.Payment;
import com.jon.chargify.payment.RequestResponseHelper;

public class CustomerRestService extends RequestResponseHelper implements ICustomerRestService {

	public Payment adjust(Payment payment) throws HttpException, IOException {

		String uri = "/subscriptions/" + payment.getSubscriptionId().intValue()
				+ "/adjustments.json";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("amount", payment.getAmount());
		paramMap.put("memo", payment.getMemo());

		Map<String, Map> params = new HashMap<String, Map>();
		params.put("adjustment", paramMap);

		PostMethod response = post(uri, params);
		String responseString = response.getResponseBodyAsString();
		Payment paymentResponse = jsonToPayment(responseString, "adjustment");

		paymentResponse.setResponseCode(response.getStatusCode());
		paymentResponse.setResponseJson(responseString);

		return paymentResponse;
	}
	
	public List<Customer> getCustomers(int page) throws HttpException, IOException {

		GetMethod method = new GetMethod("/customers.json");
		
		method.setDoAuthentication(true);
		Chargify.getClient().executeMethod(method);
		String responseString = method.getResponseBodyAsString();

		Gson gson = new Gson();
		List<Map<String, Object>> customersJsonMap = gson.fromJson(
				responseString, List.class);
		String customerJson;
		Customer customer;

		HashMap<String, String> customerMap;
		List<Customer> customers = new ArrayList<Customer>();

		int ss = method.getStatusCode();
		System.out.println(ss);

		for (Map<String, Object> customerJsonMap : customersJsonMap) {
			customerMap = (HashMap<String, String>) customerJsonMap
					.get("customer");
			customerJson = gson.toJson(customerMap);
			customer = gson.fromJson(customerJson, Customer.class);
			customers.add(customer);
		}

		return customers;
	}

}