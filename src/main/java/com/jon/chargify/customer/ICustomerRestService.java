package com.jon.chargify.customer;

import java.io.IOException;
import java.util.List;

import org.apache.commons.httpclient.HttpException;

import com.jon.chargify.Chargify;

public interface ICustomerRestService {
	
	public List<Customer> getCustomers(int page) throws HttpException, IOException;
	
}
