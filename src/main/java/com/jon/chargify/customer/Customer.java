package com.jon.chargify.customer;

public class Customer {
	private int id;
	private String last_name;
	private String email;
	private String organization;
	private String updatedAt;
	private String firstName;
	private String createdAt;

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getOrganization() {
		return organization;
	}

	public void setUpdated_at(String updated_at) {
		this.updatedAt = updated_at;
	}

	public String getUpdated_at() {
		return updatedAt;
	}

	public void setFirst_name(String first_name) {
		this.firstName = first_name;
	}

	public String getFirst_name() {
		return firstName;
	}

	public void setCreated_at(String created_at) {
		this.createdAt = created_at;
	}

	public String getCreated_at() {
		return createdAt;
	}
}