package com.jon.chargify.payment;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import com.google.gson.Gson;
import com.jon.chargify.Chargify;

public class RequestResponseHelper {

	public PostMethod post(String uri, Map<String, Map> params)
			throws HttpException, IOException {
		PostMethod method = new PostMethod(uri);
		method.setDoAuthentication(true);

		Gson gson = new Gson();
		String jsonStr = gson.toJson(params);
		RequestEntity requestEntity = new StringRequestEntity(jsonStr,
				"application/json", "UTF-8");
		method.setRequestEntity(requestEntity);

		Chargify.getClient().executeMethod(method);

		return method;
	}

	public Payment jsonToPayment(String json, String key) {
		Gson gson = new Gson();
		Map<String, Object> responseMap = gson.fromJson(json, Map.class);
		Map map = (Map) responseMap.get(key);
		String paymentString = gson.toJson(map);

		return gson.fromJson(paymentString, Payment.class);
	}

	public Payment jsonToErrors(String json, Payment payment) {
		Gson gson = new Gson();
		Map<String, Object> responseMap = gson.fromJson(json, Map.class);
		List<String> errors = (List<String>) responseMap.get("errors");
		payment.setErrors(errors);
		return payment;
	}
	
}
