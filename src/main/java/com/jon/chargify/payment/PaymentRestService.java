package com.jon.chargify.payment;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jon.chargify.Chargify;

public class PaymentRestService extends RequestResponseHelper implements
		IPaymentRestService {

	public Payment adjust(Payment payment) throws HttpException, IOException {

		String uri = "/subscriptions/" + payment.getSubscriptionId().intValue()
				+ "/adjustments.json";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("amount", payment.getAmount());
		paramMap.put("memo", payment.getMemo());

		Map<String, Map> params = new HashMap<String, Map>();
		params.put("adjustment", paramMap);

		PostMethod response = post(uri, params);
		String responseString = response.getResponseBodyAsString();
		Payment paymentResponse = jsonToPayment(responseString, "adjustment");

		if (paymentResponse == null) {
			paymentResponse = jsonToErrors(responseString, payment);
			paymentResponse.setSuccess(false);
		} else {
			paymentResponse.setSuccess(true);			
		}
		
		paymentResponse.setResponseCode(response.getStatusCode());
		paymentResponse.setResponseJson(responseString);

		return paymentResponse;
	}

	public Payment charge(Payment payment) throws HttpException, IOException {

		String uri = "/subscriptions/" + payment.getSubscriptionId().intValue()
				+ "/charges.json";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("amount", payment.getAmount());
		paramMap.put("memo", payment.getMemo());

		Map<String, Map> params = new HashMap<String, Map>();
		params.put("charge", paramMap);

		PostMethod response = post(uri, params);
		String responseString = response.getResponseBodyAsString();
		Payment paymentResponse = jsonToPayment(responseString, "charge");

		if (paymentResponse == null) {
			paymentResponse = jsonToErrors(responseString, payment);
			paymentResponse.setSuccess(false);
		} else {
			paymentResponse.setSuccess(true);			
		}

		paymentResponse.setResponseCode(response.getStatusCode());
		paymentResponse.setResponseJson(responseString);

		return paymentResponse;
	}

	public Payment refunds(Payment payment) throws HttpException, IOException {
		String uri = "/subscriptions/" + payment.getSubscriptionId().intValue()
				+ "/refunds.json";

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("payment_id", payment.getPaymentId().intValue());
		paramMap.put("amount", payment.getAmount());
		paramMap.put("memo", payment.getMemo());

		Map<String, Map> params = new HashMap<String, Map>();
		params.put("refund", paramMap);

		PostMethod response = post(uri, params);
		String responseString = response.getResponseBodyAsString();
		Payment paymentResponse = jsonToPayment(responseString, "refund");

		if (paymentResponse == null) {
			paymentResponse = jsonToErrors(responseString, payment);
			paymentResponse.setSuccess(false);
		} else {
			paymentResponse.setSuccess(true);			
		}

		paymentResponse.setResponseCode(response.getStatusCode());
		paymentResponse.setResponseJson(responseString);

		return paymentResponse;
	}

}
