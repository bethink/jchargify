package com.jon.chargify.payment;

import java.util.Date;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Payment {

	private Double id;
	private String type;

	@SerializedName("transaction_type")
	private String transactionType;
	private String kind;
	private int amount;

	@SerializedName("amount_in_cents")
	private Double amountInCents;
	private String memo;
	
	@SerializedName("ending_balance_in_cents")
	private String endingBalanceInCents;
	
	@SerializedName("subscription_id")
	private Double subscriptionId;

	@SerializedName("product_id")
	private Double productId;
	
	@SerializedName("payment_id")	
	private Double paymentId;
	
	@SerializedName("created_at")	
	private String createdAt;

	private boolean success;
	private int responseCode;
	private String responseJson;
	private List<String> errors;
	
	public Double getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Double subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Double getProductId() {
		return productId;
	}

	public void setProductId(Double productId) {
		this.productId = productId;
	}

	public Double getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Double paymentId) {
		this.paymentId = paymentId;
	}

	public Double getAmountInCents() {
		return amountInCents;
	}

	public void setAmountInCents(Double amountInCents) {
		this.amountInCents = amountInCents;
	}

	public String getEndingBalanceInCents() {
		return endingBalanceInCents;
	}

	public void setEndingBalanceInCents(String endingBalanceInCents) {
		this.endingBalanceInCents = endingBalanceInCents;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setId(Double id) {
		this.id = id;
	}

	public Double getId() {
		return id;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getMemo() {
		return memo;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getKind() {
		return kind;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseJson(String responseJson) {
		this.responseJson = responseJson;
	}

	public String getResponseJson() {
		return responseJson;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public List<String> getErrors() {
		return errors;
	}

}
