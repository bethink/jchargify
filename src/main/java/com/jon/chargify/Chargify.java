package com.jon.chargify;

import java.io.IOException;
import java.util.List;

import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.protocol.Protocol;

import com.jon.chargify.customer.Customer;
import com.jon.chargify.customer.CustomerRestService;
import com.jon.chargify.customer.ICustomerRestService;
import com.jon.chargify.payment.IPaymentRestService;
import com.jon.chargify.payment.Payment;
import com.jon.chargify.payment.PaymentRestService;

public class Chargify {

	private Protocol protocol = Protocol.getProtocol("https");
	private String hostName = "chargify.com";
	private int port = 443;
	private String subDomain;
	private String key;
	private String password = "x";
	private static HttpClient client;

	public void setDomain(String domain) {
		this.subDomain = domain;
	}

	public String getDomain() {
		return subDomain;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setClient(HttpClient client) {
		Chargify.client = client;
	}

	public static HttpClient getClient() {
		return Chargify.client;
	}

	/*
	 * subDomain: If your Site�s subdomain is google, then you would access you
	 * chargify account as google.chargify.com
	 * 
	 * key: API key
	 */
	public Chargify(String subDomain, String key) {

		this.subDomain = subDomain;
		this.key = key;

		HostConfiguration host = new HostConfiguration();
		host.setHost(subDomain + "." + hostName, port, protocol);

		if (Chargify.client == null) {
			Chargify.client = new HttpClient();
			Chargify.client.getParams().setAuthenticationPreemptive(true);

			AuthScope authScope;
			authScope = new AuthScope(subDomain + "." + hostName, port,
					"Chargify API");

			UsernamePasswordCredentials credentials;
			credentials = new UsernamePasswordCredentials(key, password);

			Chargify.client.getState().setCredentials(authScope, credentials);
			Chargify.client.setHostConfiguration(host);
		}
	}

	public List<Customer> getCustomers() {
		return getCustomers(1);
	}
	
	public List<Customer> getCustomers(int page) {
		ICustomerRestService customerService = new CustomerRestService();
		try {
			customerService.getCustomers(page);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public Payment charge(Payment payment) {

		IPaymentRestService paymentService = new PaymentRestService();

		try {
			payment = paymentService.charge(payment);
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return payment;
	}

	public Payment adjust(Payment payment) {

		IPaymentRestService paymentService = new PaymentRestService();

		try {
			payment = paymentService.adjust(payment);
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return payment;
	}

	public Payment refunds(Payment payment) {

		IPaymentRestService paymentService = new PaymentRestService();

		try {
			payment = paymentService.refunds(payment);
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return payment;
	}

}
