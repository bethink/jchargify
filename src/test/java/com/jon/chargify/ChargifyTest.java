package com.jon.chargify;

import java.util.List;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

import com.jon.chargify.customer.Customer;
import com.jon.chargify.payment.Payment;

public class ChargifyTest {

	/* Should not throw any error */
	@Test
	public void getCustomers() {

		Chargify chargify = new Chargify("crri", "-b65XTn9jFHYvyHUlfvG");
		List<Customer> customers = chargify.getCustomers();

	}

	@Test
	public void charge() {

		Chargify chargify = new Chargify("crri", "-b65XTn9jFHYvyHUlfvG");

		Payment payment = new Payment();
		payment.setAmount(42);
		payment.setMemo("from java test");
		payment.setSubscriptionId((double) 1860756);
		// 1795421
		// 1860722
		payment = chargify.charge(payment);
		// assertNotNull(payment.getAmountInCents());
		assertEquals("charge", payment.getTransactionType());
		// assertNotNull(payment.getMemo());
		// assertNotNull(payment.getCreatedAt());
		assertTrue(payment.isSuccess());
		// assertNotNull(payment.getSubscriptionId());
		// assertNotNull(payment.getId());
	}

	@Test
	public void adjust() {
		Chargify chargify = new Chargify("crri", "-b65XTn9jFHYvyHUlfvG");

		Payment payment = new Payment();
		payment.setAmount(42);
		payment.setMemo("from java test");
		payment.setSubscriptionId((double) 1827556);

		payment = chargify.adjust(payment);

		assertNotNull(payment.getAmountInCents());
		assertEquals("adjustment", payment.getTransactionType());
		assertNotNull(payment.getMemo());
		assertNotNull(payment.getCreatedAt());
		assertTrue(payment.isSuccess());
		assertNotNull(payment.getSubscriptionId());
		assertNotNull(payment.getId());
	}

	@Test
	public void refundSuccessfull() {

		Chargify chargify = new Chargify("crri", "-b65XTn9jFHYvyHUlfvG");

		Payment payment = new Payment();
		payment.setPaymentId((double) 16570360);
		payment.setAmount(1);
		payment.setMemo("Refund: from java UT");
		payment.setSubscriptionId((double) 1795421);
		Payment responsePayment = chargify.refunds(payment);

		assertEquals(responsePayment.getTransactionType(), "refund");
		assertEquals(responsePayment.getMemo(), payment.getMemo());
		assertNotNull(responsePayment.getCreatedAt());
		assertTrue(responsePayment.isSuccess());
		assertEquals(responsePayment.getSubscriptionId(),
				payment.getSubscriptionId());
		assertNotNull(responsePayment.getId());
		assertTrue(responsePayment.isSuccess());
	}

	@Test
	public void refundFailure() {

		Chargify chargify = new Chargify("crri", "-b65XTn9jFHYvyHUlfvG");

		Payment payment = new Payment();
		payment.setPaymentId((double) 16409537);
		payment.setAmount(10);
		payment.setMemo("Refund: from java UT");
		payment.setSubscriptionId((double) 1795421);
		Payment responsePayment = chargify.refunds(payment);

		assertFalse(responsePayment.isSuccess());
		assertEquals(responsePayment.getMemo(), payment.getMemo());
		assertNull(responsePayment.getCreatedAt());
		assertEquals(responsePayment.getSubscriptionId(),
				payment.getSubscriptionId());
	}
}